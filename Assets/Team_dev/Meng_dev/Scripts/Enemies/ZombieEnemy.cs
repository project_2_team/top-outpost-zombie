using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class ZombieEnemy : MonoBehaviour
{
    private PlayerHealthFull playerHealth;

    [SerializeField] private float speed;
    public Animator zombieAnimator;
    public ParticleSystem bloodEffect;
    private Transform player;
    public CapsuleCollider zombieCollider;


    //private bool isWalking = false;
    private bool isWalkingDangerZone = false;
    private bool isAttack = false;
    private bool isDie = false;

    private void Awake()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealthFull>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    private void Update()
    {
        ZombieInitialize();
    }

    private void OnTriggerEnter(Collider target)
    {
        if (isDie) return;
        if (target.gameObject.tag == "DangerZone")
        {
            //isWalking = false;
            ZombieWalkAnim(true);
            isWalkingDangerZone = true;


        }
    }

    private void OnCollisionEnter(Collision target)
    {
  
        if (isDie) return;
        if (target.gameObject.tag == "Player")
        {
           
            isAttack = true;
            SoundManager.Instance.PlayZombiehunting();
            isWalkingDangerZone = false;
            //
            
            playerHealth.TakeDamage(10);
          //  SoundManager.Instance.PlayerHurt();


        }
    }

    private void OnCollisionExit(Collision target)
    {
        if (isDie) return;

        if (target.gameObject.tag == "Player")
        {
            isAttack = false;
            isWalkingDangerZone = true;
            
        }
    }

    public void ZombieInitialize()
    {
        //OnMove(isWalking);
        OnMoveDangerZone();
        OnAttack();
    }

    public void Die()
    {
        if (isDie) return;

        //zombieCollider.enabled = false;
        isDie = true;
        //isWalking = false;
        SoundManager.Instance.PlayZombieDeath();
        isWalkingDangerZone = false;
        ZombieWalkAnim(false);
        ZombieDeathAnim();

        
        Destroy(gameObject, 2); // kill zombie
        Instantiate(bloodEffect, transform.position + Vector3.up * 1.5f, transform.rotation);
        //DestroyImmediate(gameObject, true);
    }

    //public void OnMove(bool move)
    //{
    //    if (!move) return;

    //    if (move)
    //    {
    //        ZombieWalkAnim(true);
    //        transform.position += new Vector3(0, 0, -speed * Time.deltaTime);

    //    }

    //}

    private void OnMoveDangerZone()
    {
        if(isWalkingDangerZone)
        {
            
            Vector3 oldForward = transform.forward;
            Vector3 oldPos = transform.position;
            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            Vector3 newDir = (transform.position - oldPos).normalized;
            transform.forward = Vector3.Slerp(oldForward, newDir, Time.deltaTime);
        }
    }
    public void OnAttack()
    {
        if (isAttack)
        {
            ZombieAttackAnim(isAttack);
            ZombieWalkAnim(false);

        }
        else
        {
            ZombieAttackAnim(isAttack);
        }

    }

    // Zombie Anim
    public void ZombieWalkAnim(bool isWalking)
    {
        if (isWalking)
            zombieAnimator.SetBool("Walking", true);
        else
            zombieAnimator.SetBool("Walking", false);
    }

    public void ZombieAttackAnim(bool isAttacking)
    {
        if (isAttacking)
            zombieAnimator.SetBool("Attacking", true);
        else
            zombieAnimator.SetBool("Attacking", false);
    }

    public void ZombieDeathAnim()
    {
        zombieAnimator.SetBool("Death", true);
    }

}
