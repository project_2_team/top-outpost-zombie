using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawn : MonoBehaviour
{
    public GameObject zombiePrefab;
    private Transform parentTransform;
    private Vector3 newPos;
    private Quaternion newRotation;

    private GameObject[] zombies;
    public int count = 0;

    private void Start()
    {
        zombies = new GameObject[100];
  
    }

    private void Update()
    {
        SpawnZombie();
        count++;

    }

    private void SpawnZombie()
    {
        if(count >= 0 && count < 100)
        {
            StartCoroutine(ZombieSpawnRoutine());

        }else if(count == 100)
        {
            StopCoroutine(ZombieSpawnRoutine());
        }
       
        
    }


    private IEnumerator ZombieSpawnRoutine()
    {
        yield return new WaitForSeconds(1f);

        Vector3 randomZombie = new Vector3(Random.Range(-5, 5), 0, Random.Range(-70, 70));
        Instantiate(zombiePrefab, randomZombie, newRotation, parentTransform);
       
    }

}
