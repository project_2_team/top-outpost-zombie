using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveOnRoad : MonoBehaviour
{

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SoundManager.Instance.PlayerDeath();
        }
    }
}
