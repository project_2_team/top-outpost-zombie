using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnderGround : MonoBehaviour
{


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Zombie"))
        {
            //Debug.Log("Mewng");
            Destroy(collision.gameObject);
            

        }

        if (collision.gameObject.CompareTag("Player"))
        {
            SoundManager.Instance.PlayerDeath();
            SceneManager.LoadScene("GameOver");
        }
    }

}
