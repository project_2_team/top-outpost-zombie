using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    public Animator animator;
    private PlayerInput playerInput;


    private void Awake()
    {
        playerInput = new PlayerInput();

    }

    private void OnEnable()
    {
        playerInput.Enable();
    }

    private void OnDisable()
    {
        playerInput.Disable();
    }

    public void OnMove()
    {
        Vector2 move = playerInput.Player.Movement.ReadValue<Vector2>();
        //Debug.Log($"move = {move}");
        transform.position += new Vector3(move.x, 0, move.y) * speed * Time.deltaTime;

        //Debug.Log($"transform.position = {transform.position}");
        /*        if (move != Vector2.zero)
                {
                    PlayerMoveAnim(true);
                    SoundManager.Instance.PlayWalking();

                }
                if(move ==  Vector2.zero)
                {
                    PlayerMoveAnim(false);

                }*/


        if (animator.GetBool("Moving") != (move != Vector2.zero))
        {
            animator.SetBool("Moving", move != Vector2.zero);


        }
    }

    //public void PlayerMoveAnim(bool playerMove)
    //{
    //    if (playerMove)
    //        animator.SetBool("Moving", true);

    //    else
    //        animator.SetBool("Moving", false);

    //}

}
