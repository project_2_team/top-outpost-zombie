using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerController : MonoBehaviour
{
    private PlayerMove playerMove;

    [Header("Gun")]
    public Transform gunFront;
    public GameObject bullet;
    public ParticleSystem shootingEffect;
    

    private void Awake()
    {
        playerMove = GetComponent<PlayerMove>();
    }

    private void Update()
    {
        playerMove.OnMove();

    }

    public void Fire()
    {

        GameObject newBullet = Instantiate(bullet);

        newBullet.transform.rotation = transform.rotation;
        newBullet.transform.position = gunFront.position;

        MyBullet bulletController = newBullet.GetComponent<MyBullet>();
        bulletController.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        bulletController.player = this;
        shootingEffect.Play();

        SoundManager.Instance.PlayShooter();

        Destroy(bulletController.gameObject, 2);
    }


}
