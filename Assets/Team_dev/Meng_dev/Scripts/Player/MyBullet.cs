using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyBullet : MonoBehaviour
{

    public float force;

    [HideInInspector]
    public MyPlayerController player;

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * force);
    }
    private void OnTriggerEnter(Collider target)
    {
        if (!target.gameObject.CompareTag("Zombie"))
            return;

        if (target.gameObject.CompareTag("Zombie"))
        {

            target.gameObject.GetComponent<ZombieEnemy>().Die();
            Destroy(this.gameObject); // destroy this bullet
        }
        
    }


}
