using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Slider musicSlider, sfxSlider;

    public void ToggleMusic()
    {
        SoundManager.Instance.PlayShooter();
    }
    public void ToggleSFX()
    {
        SoundManager.Instance.PlayWalking();
    }
    public void MusicVolume()
    {
        SoundManager.Instance.MusicVolume(musicSlider.value);
    }
    public void SFXVolume()
    {
        SoundManager.Instance.SFXVolume(sfxSlider.value);
    }

}
