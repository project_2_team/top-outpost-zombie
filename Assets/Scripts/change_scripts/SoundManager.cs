using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioSource soundEffectSource;
    public AudioSource musicSource;

    // Add your sound effect clips here
    public AudioClip walking;
    public AudioClip Shooting;
    public AudioClip gameWinner;
    public AudioClip zombieDeath;
    public AudioClip zombieHuting;
    public AudioClip playerHurt;
    public AudioClip playerDeath;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
    }

    public void PlayWalking()
    {
        soundEffectSource.clip = walking;
        soundEffectSource.Play();
    }
    public void PlayZombieDeath()
    {
        musicSource.clip = zombieDeath;
        musicSource.Play();
    }
    public void PlayerHurt()
    {
        musicSource.clip = playerHurt;
        musicSource.Play();
    }
    public void PlayerDeath()
    {
        musicSource.clip = playerDeath;
        musicSource.Play();
    }
    public void PlayZombiehunting()
    {
        musicSource.clip = zombieHuting;
        musicSource.Play();
    }

    public void PlayShooter()
    {
        soundEffectSource.clip = Shooting;
        soundEffectSource.Play();
    }

    public void PlayGameWinner()
    {
        musicSource.clip = gameWinner;
        musicSource.Play();
    }
    public void PlaySoundEffect(AudioClip clip)
    {
        soundEffectSource.clip = clip;
        soundEffectSource.Play();
    }
    public void ToggleMusic()
    {
        musicSource.mute = !musicSource.mute;
    }
    public void ToggleSFX()
    {
        soundEffectSource.mute = !soundEffectSource.mute;
    }
    public void MusicVolume(float volume)
    {
        musicSource.volume = volume;
    }
    public void SFXVolume(float volume)
    {
        soundEffectSource.volume = volume;
    }
}
