using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class GameManegement : MonoBehaviour
{
    
    public string NameLevelNext;

    private IEnumerator NextLevel()
    {
        
        WaitForSeconds next = new WaitForSeconds(2f);
        yield return next;
        SceneManager.LoadScene(NameLevelNext);
    }
    public void StartGameManager()
    {
        SceneManager.LoadScene("Change_Level1_Meng");
    }
    public void Quit()
    {
        Application.Quit();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            SoundManager.Instance.PlayGameWinner();
            StartCoroutine(NextLevel());
        }
    }

}
