using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class PlayerHealthFull : MonoBehaviour
{
    [Header("Health")]
    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    public void TakeDamage(int damage)
    {

        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        StartCoroutine(PlayerDeath());
    }

    private IEnumerator PlayerDeath()
    {
        if(currentHealth <= 0)
        {
            SoundManager.Instance.PlayerDeath();
            WaitForSeconds next = new WaitForSeconds(1.5f);
            yield return next;
            SceneManager.LoadScene("GameOver");
        }
    }
}
