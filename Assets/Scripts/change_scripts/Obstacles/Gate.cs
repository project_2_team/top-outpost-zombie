﻿using TMPro;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public TextMeshPro textPro;
    public int currentText;
    public HealthBar healthBar;
    private PlayerHealthFull health;
    private BoxCollider boxCollider;
    


    private void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        health = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealthFull>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            health.currentHealth += currentText;
            textPro.text = " " + currentText;
            healthBar.SetHealth(health.currentHealth);
            Destroy(gameObject);
            boxCollider.enabled = false;
            
        }
    }
}